module ApplicationHelper
  def in_admin?
    controller.class.name.split("::").first=="Admin"
  end
end
