class MenuItem < ActiveRecord::Base
  has_closure_tree
  validates :title, presence: true

  def first_link
    return self.link unless self.link.blank?
    self.children.where("link is not null").first.try(:link)
  end
end
