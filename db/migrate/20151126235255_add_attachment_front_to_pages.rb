class AddAttachmentFrontToPages < ActiveRecord::Migration
  def self.up
    change_table :pages do |t|
      t.attachment :front
    end
  end

  def self.down
    remove_attachment :pages, :front
  end
end
