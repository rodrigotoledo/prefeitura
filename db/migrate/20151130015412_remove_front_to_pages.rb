class RemoveFrontToPages < ActiveRecord::Migration
  def change
    remove_column :pages, :front_file_name
    remove_column :pages, :front_content_type
    remove_column :pages, :front_file_size
    remove_column :pages, :front_updated_at
  end
end
